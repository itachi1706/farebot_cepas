# FareBot CEPAS

View your remaining balance, recent trips, and other information from contactless public transit cards using your NFC Android phone!

Note: This is just a cut down version of the original [FareBot Application](https://github.com/codebutler/farebot). If you wish for something more than CEPAS support head over there instead!

## Written By

* [Eric Butler][3] <eric@codebutler.com>

## Code Cutdown By

* [Kenneth Soh][0] <itachi1706@itachi1706.com>

## Thanks To

* [Sean Cross][2] (CEPAS/EZ-Link)
* [tbonang](https://github.com/tbonang) (NETS FlashPay)

## License

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Supported Protocols

* [CEPAS][1] (Not compatible with all devices)

## Supported Cards

* [EZ-Link][4] - Singapore (Not compatible with all devices)
* [NETS FlashPay](http://www.netsflashpay.com.sg/) - Singapore

## Supported Phones

FareBot CEPAS requires an NFC Android phone running 5.0 or later.

## Building

    $ git clone https://gitlab.com/itachi1706/farebot_cepas.git
    $ cd farebot_cepas
    $ ./gradlew assembleDebug

## Open Source Libraries

FareBot uses the following open-source libraries:

* [AutoDispose](https://github.com/uber/AutoDispose)
* [AutoValue](https://github.com/google/auto/tree/master/value)
* [AutoValue Gson](https://github.com/rharter/auto-value-gson)
* [Dagger](https://google.github.io/dagger/)
* [Gson](https://github.com/google/gson)
* [Guava](https://github.com/google/guava)
* [Kotlin](https://kotlinlang.org/)
* [Magellan](https://github.com/wealthfront/magellan/)
* [RxBroadcast](https://github.com/cantrowitz/RxBroadcast)
* [RxJava](https://github.com/ReactiveX/RxJava)
* [RxRelay](https://github.com/JakeWharton/RxRelay)

[0]: https://twitter.com/#!/itachi1706
[1]: https://en.wikipedia.org/wiki/CEPAS
[2]: https://twitter.com/#!/xobs
[3]: https://twitter.com/#!/codebutler
[4]: http://www.ezlink.com.sg/
